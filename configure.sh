#!/bin/bash

# quickstart configuration script
# jw@dataconnect.io
echo
echo "Welcome to the DataConnect.io configuration script."
echo "Please make sure that you have the following at the ready:"
echo "1) a DataConnect.io account with a consumer id and secret"
echo "2) an app on the remote platform, e.g. https://developers.shopify.com/"
echo
read -e -p "Enter the DataConnect.io consumer id: " -i "" CONSUMERID
read -e -p "Enter the DataConnect.io consumer secret: " -i "" CONSUMERSECRET

sed "s/secretsecret/$CONSUMERSECRET/g" dist/quickstart/dist/conf/app.yml.template > dist/quickstart/dist/conf/app.yml
sed -i "s/c679cbe7-1937-46c5-b995-c9ac1f6266e8/$CONSUMERID/g" dist/quickstart/dist/conf/app.yml

echo "Determining your public IP address..."

IP=$(curl ipinfo.io/ip)

echo "Found $IP"
echo "Setting the redirect URL where users will be redirected to after the connection is established (probably a success page in your application)."
echo "If you are using the demo app, http://yourip/demo will work fine (assuming firewalls are configured etc.)"
read -e -p "Enter the redirect URL: " -i http://$IP:8081/data applicationRedirectUri

echo $applicationRedirectUri
sed -i "s#APPREDIRECTURI#$applicationRedirectUri#g" dist/quickstart/dist/conf/app.yml
sed "s#PUBLICIP#$IP#g" dist/quickstart/dist/conf/app.yml
sed "s#PUBLICIP#$IP#g" dist/quickstart/dist/conf/demo.yml

echo "Setting the URL where the quickstart app is publicly available, used to handle sign-up flows and receive data streams as RESThooks."
echo "This will be used by the demo app to initiate sign-ups."
echo "If you are using the quickstart app, http://yourip:8082 will work fine (assuming firewalls are configured etc.)"
read -e -p "Enter the quickstart app URL: " -i http://$IP:8082 quickstartAppUrl

echo $quickstartAppUrl
sed "s#QSAPPURI#$quickstartAppUrl#g" dist/quickstart/dist/conf/demo.yml.template > dist/quickstart/dist/conf/demo.yml
echo "Thank you. That should be it."
echo "Now run docker-compose up."
echo "On the first run, run `sh dist/mongo/init-mongo.sh zxis7xPZPM8hGkTQpQRB` to initialize the mongodb collections."


echo "To give the demo a try, visit http://$IP:8081".

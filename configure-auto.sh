#!/bin/bash

# quickstart configuration script
# jw@dataconnect.io
echo
echo "Welcome to the DataConnect.io auto-configuration script."
echo

# Get one of the prepared consumers

## Array with consumers
consumers=("c0f8f864-eeb2-11e6-90ab-d9ae5db0bf2d" "a4aa6abb-eeb2-11e6-90ab-e31f09b8be96" "44d60ec9-eeb1-11e6-90ab-dbef48c5fdf5")

## Seed random generator
RANDOM=$$$(date +%s)

BLOBID=${consumers[$RANDOM % ${#consumers[@]} ]}

URL="https://jsonblob.com/api/jsonBlob/$BLOBID"
BLOB=$(curl -s $URL)
CONSUMERID=$(echo $BLOB | jq -r '.id')
CONSUMERSECRET=$(echo $BLOB | jq -r '.secret')
CONSUMERKEY=$(echo $BLOB | jq -r '.key')
echo $CONSUMERID

# generate jwt token for auth later
JWT=$(jwt3 --alg=HS256 --key=$CONSUMERSECRET iss=$CONSUMERKEY aud="http://dataconnect.io/" exp=+36000)
JWT=$(echo $JWT | sed "s/b'//g" | sed "s/'//g")

touch dist/quickstart/dist/conf/app.yml
touch dist/quickstart/dist/conf/demo.yml

sed "s/secretsecret/$CONSUMERSECRET/g" dist/quickstart/dist/conf/app.yml.template > dist/quickstart/dist/conf/app.yml
sed -i "s/c679cbe7-1937-46c5-b995-c9ac1f6266e8/$CONSUMERID/g" dist/quickstart/dist/conf/app.yml

echo "Determining your public IP address..."

IP=$(curl -s ipinfo.io/ip)
echo "Found $IP"
echo

echo "Setting the redirect URL where users will be redirected to after the connection is established (probably a success page in your application)."
echo "If you are using the demo app, http://yourip/demo will work fine (assuming firewalls are configured etc.)"
applicationRedirectUri="http://$IP/data"

echo "Redirect URI after sign up set to: $applicationRedirectUri"
sed -i "s#APPREDIRECTURI#$applicationRedirectUri#g" dist/quickstart/dist/conf/app.yml
echo
echo "Setting the URL where the quickstart app is publicly available, used to handle sign-up flows and receive data streams as RESThooks."
echo "This will be used by the demo app to initiate sign-ups."
echo "If you are using the quickstart app, http://yourip:8082 will work fine (assuming firewalls are configured etc.)"
quickstartAppUrl="http://$IP:8082"

echo "QuickStartApp is set to: $quickstartAppUrl"

sed "s#QSAPPURI#$quickstartAppUrl#g" dist/quickstart/dist/conf/demo.yml.template > dist/quickstart/dist/conf/demo.yml

sed -i "s#PUBLICIP#$IP#g" dist/quickstart/dist/conf/app.yml
sed -i "s#PUBLICIP#$IP#g" dist/quickstart/dist/conf/demo.yml

SETTINGSURL="https://api2.dataconnect.io/v2/platforms/shopify/admin/consumers/$CONSUMERID/settings"
SETTINGS=$(curl -s $SETTINGSURL -H "Authorization: Bearer $JWT")
echo $SETTTINGS
apiKey=$(echo $SETTINGS | jq -r '.apiKey')
apiSecret=$(echo $SETTINGS | jq -r '.apiSecret')
appUrl=$(echo $SETTINGS | jq -r '.appUrl')
scopes=$(echo $SETTINGS | jq -r '.scopes')
finalRedirectUrl="$quickstartAppUrl/connect/shopify/finish"
restHookUrl="$quickstartAppUrl/rest"

request_body=$(jq -n --arg scopes "$scopes" --arg apiSecret "$apiSecret" --arg appUrl "$appUrl" --arg finalRedirectUrl "$finalRedirectUrl" --arg restHookUrl "$restHookUrl" --arg apiKey "$apiKey" '
{
    "apiKey": $apiKey, 
    "apiSecret": $apiSecret, 
    "appUrl": $appUrl, 
    "finalRedirectUrl": $finalRedirectUrl, 
    "restHookUrl": $restHookUrl, 
    "scopes": $scopes
}'
)

status=$(curl --write-out %{http_code} --silent --output /dev/null -H "Accept: application/json" -H "Content-Type:application/json" -X PUT --data "$request_body" $SETTINGSURL -H "Authorization: Bearer $JWT")

if [ $status -ge 400  ]
  then 
      echo "Sorry, encountered an error at $url with status $status"
      exit -1
fi
if [ $status -eq 200  ]
  then
      echo "Connector configuration done."
fi

echo
echo
echo "Thank you. That should be it (almost)."
echo "1. run docker-compose up."
echo "2. On the first run, run sh dist/mongo/init-mongo.sh zxis7xPZPM8hGkTQpQRB to initialize the mongodb collections."
echo "3. To give the demo a try, visit http://$IP".

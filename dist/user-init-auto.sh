#!/bin/bash
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
sudo apt-get update
sudo apt-get install -y docker-engine jq nginx-light

curl -o /usr/local/bin/docker-compose -L "https://github.com/docker/compose/releases/download/1.10.1/docker-compose-$(uname -s)-$(uname -m)"
chmod +x /usr/local/bin/docker-compose

git clone https://gitlab.com/dataconnect.io/quickstart.git /root/quickstart
cd /root/quickstart
git checkout feature-configure-script

docker-compose pull

./configure-auto.sh

docker-compose up -d

cp /root/quickstart/dist/nginx.conf /etc/nginx/sites-enabled/default 
sudo service nginx restart

sleep 10

sh dist/mongo/init-mongo.sh

IP=$(curl -s ipinfo.io/ip)
sudo wall -n "Reax! To give the demo a try, visit http://$IP".

#!/bin/bash

PWD=zxis7xPZPM8hGkTQpQRB

until curl -f --user quickstart:$PWD -XPUT http://127.0.0.1:8080/quickstart; do
   echo "try again..."
   sleep 5
done

for COLLECTION in "connections" "stores" "products" "orders" "products_raw" "orders_raw" "stores_raw"
do
        curl --user quickstart:$PWD -XPUT http://127.0.0.1:8080/quickstart/$COLLECTION
done

exit 0

# DataConnect.io Quickstart App

This application provides a very easy way to start using DataConnect.io services. It is executed as an independent 
application that implements the signup flow and RestHook endpoints for any of our supported platforms.

##Try it on Dply

By clicking the button below, you can get started easily
- you'll get a cloud VPS 2 hours for free
- user-init script to set up docker and clone the necessary repository to /quickstart

[![Deploy directly on Dply](https://dply.co/b.svg)](https://dply.co/b/RGDFhXlA)

## Deploy to your server

### Requirements

To run QuickStart App and its add-ons you need:

- DockerEngine v1.12+ ([installation guide](https://docs.docker.com/engine/installation/))
- DockerCompose compatible with Docker v1.12+ ([installation guide](https://docs.docker.com/compose/install/))

### How to use

1. `git clone https://gitlab.com/dataconnect.io/quickstart.git`
1. Run ./configure and follow the directions, consult the docs if necessary:
  - [Configure Docs](https://gitlab.com/dataconnect.io/quickstart/wikis/configuration) 
1. `docker-compose up` (or `docker-compose up -d` as a background task)

**Running locally**

QuickStart App can be used locally if you will expose it publicly, for example, using [ngrok](https://ngrok.com/docs#expose)).  
